
import 'package:flutter/cupertino.dart';

class BottomNavigationProvider extends ChangeNotifier{

  int _currentIndex = 0;

  get currentIndex =>_currentIndex;

  set currentIndex(int index){
    this._currentIndex = index;
    notifyListeners();
  }


}